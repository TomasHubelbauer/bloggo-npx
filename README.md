# NPX

It's supposed to ship with NPM itself: https://github.com/npm/npm/releases/tag/v5.2.0
But in my case I had to install it globally.

- [ ] Find out why it didn't install with NPM for me - is it because of NVM for Windows?

NPX replaces this:

- Installing things globally (what if you need two versions?)
- Invoking things from `node-modules/.bin` (it's too chatty)
- Invoking things using `scripts`
    - In the script body `node-modules/.bin/x` can be used
    - Also directly `x` can be used as NPM `scripts` understand `node-modules/.bin`
- Creating bash files and shit
- `npx x` invokes `x` in `node-modules/.bin`
- `x` can work directly with some shell fallback things
    - Explore this in WSL - maybe also in PowerShell?
